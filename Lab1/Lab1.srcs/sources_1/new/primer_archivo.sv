`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.05.2020 15:34:33
// Design Name: 
// Module Name: primer_archivo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module primer_archivo(

    );
endmodule

module cable(input logic A, output logic B);

    assign B = A;

endmodule 

module logica_simple(input logic A,B,C, output logic X,Y,Z);

    assign X = A;
    assign Y = ~A;
    assign Z = B & C;
endmodule


