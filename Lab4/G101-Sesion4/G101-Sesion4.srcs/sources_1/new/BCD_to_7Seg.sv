`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2020 17:49:02
// Design Name: 
// Module Name: BCD_to_7Seg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module BCD_to_7Seg(
    input logic [3:0] BCD_in,
    output logic [6:0] sevenSeg
    );
    logic [6:0] sevenSeg_aux;
    always_comb begin
        case(BCD_in)          //gfedcba
            4'd0: sevenSeg_aux  = 7'b1000000;    //0
            4'd1: sevenSeg_aux  = 7'b1111001;    //1
            4'd2: sevenSeg_aux  = 7'b0100100;    //2
            4'd3: sevenSeg_aux  = 7'b0110000;    //3
            4'd4: sevenSeg_aux  = 7'b0011001;    //4
            4'd5: sevenSeg_aux  = 7'b0010010;    //5
            4'd6: sevenSeg_aux  = 7'b0000010;    //6
            4'd7: sevenSeg_aux  = 7'b1111000;    //7
            4'd8: sevenSeg_aux  = 7'b0000000;   //8
            4'd9: sevenSeg_aux  = 7'b0011000;   //9
            4'd10: sevenSeg_aux = 7'b0001000;   //a
            4'd11: sevenSeg_aux = 7'b0000011;   //b
            4'd12: sevenSeg_aux = 7'b1000110;   //c
            4'd13: sevenSeg_aux = 7'b0100001;   //d
            4'd14: sevenSeg_aux = 7'b0000110;   //e
            4'd15: sevenSeg_aux = 7'b0001110;   //f
        endcase
    end
    
    assign sevenSeg = {sevenSeg_aux[0],sevenSeg_aux[1],sevenSeg_aux[2],sevenSeg_aux[3],
                        sevenSeg_aux[4],sevenSeg_aux[5],sevenSeg_aux[6]};
endmodule

