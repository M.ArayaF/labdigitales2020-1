`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2020 17:39:51
// Design Name: 
// Module Name: Mux_display
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// Determinara que es lo que se representara en el 7-segmentos seleccionado por el syncronizador
module Mux_display(
    input logic [2:0] control_sync,
    input logic [31:0] valores,
    output logic [3:0] out);
    
    always_comb begin
        case(control_sync)
            3'b000 : out = valores[3:0];
            3'b001 : out = valores[7:4];
            3'b010 : out = valores[11:8];
            3'b011 : out = valores[15:12];
            3'b100 : out = valores[19:16];
            3'b101 : out = valores[23:20];
            3'b110 : out = valores[27:24];
            3'b111 : out = valores[31:28];
        endcase
    end

endmodule

