`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2020 17:02:11
// Design Name: 
// Module Name: S4_Actividad2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module S4_Actividad2(
    input logic clock_100M, 
    input logic reset,
    output logic clock_out_50M, //1
    output logic clock_out_30M, //1.6
    output logic clock_out_10M, // 5
    output logic clock_out_1M); //50
    
    clock_divider #(.f_clk_out(50)) clock_1 (.clk_in(clock_100M), .reset(reset), .clk_out(clock_out_50M));
    clock_divider #(.f_clk_out(30)) clock_2 (.clk_in(clock_100M), .reset(reset), .clk_out(clock_out_30M));
    clock_divider #(.f_clk_out(10)) clock_3 (.clk_in(clock_100M), .reset(reset), .clk_out(clock_out_10M));
    clock_divider #(.f_clk_out(1))  clock_4 (.clk_in(clock_100M), .reset(reset), .clk_out(clock_out_1M));
        
endmodule

