`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2020 17:02:11
// Design Name: 
// Module Name: S4_Actividad3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S4_Actividad3(
    input logic [1:0] OpCode,
    input logic [7:0] A,
    input logic [7:0] B,
    output logic [7:0] Result,
    output logic [3:0] Status);

    ALU #(.N(8),.M(2)) U_ALU (.OpCode(OpCode), .A(A), .B(B), .Result(Result), .Status(Status));

endmodule