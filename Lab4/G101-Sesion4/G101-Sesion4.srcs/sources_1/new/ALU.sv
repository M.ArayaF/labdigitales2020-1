`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2020 21:24:26
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU #(
    parameter N=8,
    parameter M=2)(
    input logic [M-1:0] OpCode,
    input logic [N-1:0] A, B,
    output logic [N-1:0] Result,
    output logic [3:0] Status
    );
    logic n,z,c,v;
    logic [N:0] Result_aux;
    
    always_comb begin
        case (OpCode)
            2'b00: Result_aux = A + B;
            2'b01: Result_aux = A - B; 
            2'b10: Result_aux = A | B;
            2'b11: Result_aux = A & B;
            default: Result_aux= {N{1'b0}};
        endcase
        
        if (Result_aux[7] == 1'b1)
            n = 1'b1;
        else
            n = 1'b0;
    
        if (Result_aux[N-1:0] == {N{1'b0}}) //'0
            z = 1'b1;
        else
            z = 1'b0;
            
        if (Result_aux[8] == 1'b1) // 8'b11111111
            c = 1'b1;
        else
            c = 1'b0;
        
        if (A[N-1] == 1'b0 && B[N-1] == 1'b0 && OpCode == 2'b00) begin //Ambos positivos sumando
            if (Result_aux[N-1] == 1'b0) begin //Resultado positivo
                v = 1'b0;  //No overflow
            end
            else begin
                v = 1'b1; //Overflow
            end
        end
        else begin
            if (A[N-1] == 1'b1 && B[N-1] == 1'b1 && OpCode == 2'b00) begin // Ambos negativos sumando
                if (Result_aux[N-1] == 1'b1) begin  // Resultado negativo
                    v = 1'b0; //  no overflow 
                end
                else begin 
                    v = 1'b1; // overflow
                end
            end
            else begin
                if(A[N-1] == B[N-1]) begin
                    if(A <= B) begin
                        
                    end
                    else begin
                    end
                end
                else begin
                    if(OpCode == 2'b01 && Result_aux[N-1] != A[N-1]) begin
                        v = 1'b1;
                    end
                    else begin
                        v = 1'b0; // no overflow
                    end
                end
            end
        end
    end 
    
    assign Status = {n,z,c,v};
    assign Result = Result_aux[N-1:0];
    
endmodule