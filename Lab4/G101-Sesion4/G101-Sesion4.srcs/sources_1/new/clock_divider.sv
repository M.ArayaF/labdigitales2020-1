`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2020 20:07:26
// Design Name: 
// Module Name: clock_divider
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clock_divider
#(parameter f_clk_out = 1) 
 (input logic clk_in,
  input logic reset,
  output logic clk_out = 1);
  
  localparam COUNTER_MAX = int'((100_000_000.0/(2.0*f_clk_out)));
  localparam DELAY_WIDTH = $clog2(COUNTER_MAX);  // The $clog2 function returns the ceiling(el nmero entero mayor, ms cercano) of the logarithm to the base 2.
  logic [DELAY_WIDTH-1:0] counter = 'd0;
  
  // Resetea el contador e invierte el valor del reloj de salida
  // cada vez que el contador llega a su valor maximo.
  
  always_ff @(posedge clk_in) begin
    if (reset == 1'b1) begin
    // Reset sincronico, setea el contador y la salida a un valor conocido
      counter <= 'd0;
      clk_out <= 0;
    end
    else if (counter == COUNTER_MAX -1) begin
     // Se resetea el contador y se invierte la salida 
           counter <= 'd0;
           clk_out <= ~clk_out;
    end 
    else begin
    // se incrementa el contador y se mantiene la salida con su valor anterior
      counter <= counter + 'd1;
      clk_out <= clk_out;
    end
  end
endmodule

