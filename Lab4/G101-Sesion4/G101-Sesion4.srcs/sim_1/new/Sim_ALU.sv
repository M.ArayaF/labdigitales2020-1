`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2020 21:26:33
// Design Name: 
// Module Name: Sim_ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sim_ALU();
    logic [1:0] tipoOperacion;
    logic [7:0] operando1;
    logic [7:0] operando2;
    logic [7:0] resultado_ALUteam;
    logic [7:0] resultado_ALUref;
    logic [3:0] status_ALUteam;
    logic [3:0] status_ALUref;

    ALU AluTest (.OpCode(tipoOperacion), .A(operando1), .B(operando2), .Result(resultado_ALUteam), .Status(status_ALUteam));
    ALU_ref ALUref (.OpCode(tipoOperacion), .A(operando1), .B(operando2), .Result(resultado_ALUref), .Status(status_ALUref));
    
    

    initial begin
        operando1 = 8'b1111_1101;
        operando2 = 8'b1111_1100;
        tipoOperacion = 2'b00;
        #10 
        operando1 = 8'b1100_0000;
        operando2 = 8'b1010_0000;
        tipoOperacion = 2'b00;
        #10 
        operando1 = 8'b1110_0000;
        operando2 = 8'b1100_0000;
        tipoOperacion = 2'b00;
        #10 
        operando1 = 8'b0101_0000;
        operando2 = 8'b0110_0000;
        tipoOperacion = 2'b00;
        #10 
        operando1 = 8'b0000_0001;
        operando2 = 8'b0000_0001;
        tipoOperacion = 2'b01;
        #10 
        operando1 = 8'b1110_1011;
        operando2 = 8'b1101_0101;
        tipoOperacion = 2'b01; 
        #10 
        operando1 = 8'b1100_1101;
        operando2 = 8'b0111_0101;
        tipoOperacion = 2'b01;         
        #10
        operando1 = 8'b0111_0001;
        operando2 = 8'b0110_1101;
        tipoOperacion = 2'b10;
        #10
        operando1 = 8'b1100_0101;
        operando2 = 8'b0110_1101;
        tipoOperacion = 2'b10;
        #10
        operando1 = 8'b0001_0001;
        operando2 = 8'b1110_1101;
        tipoOperacion = 2'b10;
        #10
        operando1 = 8'b0111_1101;
        operando2 = 8'b1110_0101;
        tipoOperacion = 2'b10;
        #10
        operando1 = 8'b1111_0101;
        operando2 = 8'b1100_1100;
        tipoOperacion = 2'b11;
        #10
        operando1 = 8'b1101_0101;
        operando2 = 8'b1101_1100;
        tipoOperacion = 2'b11;
        #10
        operando1 = 8'b1111_0101;
        operando2 = 8'b0101_1111;
        tipoOperacion = 2'b11;
        #10
        operando1 = 8'b0011_1101;
        operando2 = 8'b1110_1100;
        tipoOperacion = 2'b11;
        #10
        operando1 = 8'b1111_0101;
        operando2 = 8'b1110_1101;
        tipoOperacion = 2'b11;
        #10
        operando1 = 8'b1111_0101;
        operando2 = 8'b0000_0000;
        tipoOperacion = 2'b11;
    end
endmodule
