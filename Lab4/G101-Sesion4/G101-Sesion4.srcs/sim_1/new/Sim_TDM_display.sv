`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2020 18:04:18
// Design Name: 
// Module Name: Sim_TDM_display
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sim_TDM_display();
    logic clk;
    logic reset;
    logic[31:0] BCD_in;
    logic [6:0] segments; // CA,CB,CC,CD,CE,CF,CG;
    logic [7:0] anodos; //AN7, AN6, AN5, AN4, AN3, AN2, AN1, AN0;
    
    S4_Actividad1 D8_Seg(
    .clock(clk),
    .reset(reset),
    .BCD_in(BCD_in),
    .segments(segments),
    .anodos(anodos)
    );
    
    always #5 clk =~clk;
    
    initial begin
    clk = 1;
    reset = 1;
    BCD_in = 32'h76543210;
    #15 
        reset = 0;
        BCD_in = 32'h76543210;
    #80
        BCD_in = 32'hfedcba98;
    end
endmodule

