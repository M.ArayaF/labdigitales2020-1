`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2020 17:03:26
// Design Name: 
// Module Name: Sim_Clock_frec_divider
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sim_Clock_frec_divider();
    logic clock_100M;
    logic clock_out_50M, clock_out_30M, clock_out_10M, clock_out_1M;
    logic reset;
    S4_Actividad2 Clk_Divider
                (.clock_100M(clock_100M), .reset(reset),
                .clock_out_50M(clock_out_50M), .clock_out_30M(clock_out_30M), 
                .clock_out_10M(clock_out_10M), .clock_out_1M(clock_out_1M));
    
    always #5 clock_100M =~clock_100M; //100MHz
    
    initial begin
        clock_100M = 0;
        reset = 1;
        #10
        reset = 0; 
    end
    
endmodule

