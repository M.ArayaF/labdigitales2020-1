`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.05.2020 16:51:33
// Design Name: 
// Module Name: lab2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab2(
    
    );
endmodule
module test_counter();
    logic clk, reset;
    logic[0:3] count;
    logic [6:0] seven;
    logic f_out;
    assign O_I = 1'b0;
    
    counter_4bits DUT(.clk(clk), .reset(reset), .count(count));
    BCD_to_Seven_Seg Seven_Seg( .BCD_in(count), .sevenSeg(seven));
    fib_procedural FIB(.BCD_in(count),.f(f_out));
    always #5 clk = ~clk;
    
    initial begin
        clk = 0;
        reset = 1;
        #10
        reset = 0;
    end
endmodule
