`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.05.2020 17:40:42
// Design Name: 
// Module Name: BCD_to_Seven_Seg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BCD_to_Seven_Seg(
    input logic [3:0] BCD_in,
    output logic [6:0] sevenSeg
    //,output logic [6:0] anodoSeg
    );
    always_comb begin
        case(BCD_in)          //gfedcba
            4'd0: sevenSeg = 7'b1000000;    //0
            4'd1: sevenSeg = 7'b1111001;    //1
            4'd2: sevenSeg = 7'b0100100;    //2
            4'd3: sevenSeg = 7'b0110000;    //3
            4'd4: sevenSeg = 7'b0011001;    //4
            4'd5: sevenSeg = 7'b0010010;    //5
            4'd6: sevenSeg = 7'b0000010;    //6
            4'd7: sevenSeg = 7'b1111000;    //7
            4'd8: sevenSeg = 7'b0000000;   //8
            4'd9: sevenSeg = 7'b0011000;   //9
            4'd10: sevenSeg = 7'b0001000;   //a
            4'd11: sevenSeg = 7'b0000011;   //b
            4'd12: sevenSeg = 7'b1000110;   //c
            4'd13: sevenSeg = 7'b0100001;   //d
            4'd14: sevenSeg = 7'b0000110;   //e
            4'd15: sevenSeg = 7'b0001110;   //f
            default: sevenSeg = 7'b1111111;
        endcase
    end
endmodule
