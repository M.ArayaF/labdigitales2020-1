`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.07.2020 21:46:12
// Design Name: 
// Module Name: Sesion7_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module actividad1();
    logic            clk, resetN;
    logic            Enter;
    logic    [15:0]  DataIn;
    logic    [3:0]   Flags;
    logic    [15:0]  ToDisplay;
    logic    [1:0]   CurrentState;
    
    Act1_RPCalculator#(10) act1(.clk(clk), .resetN(resetN), .Enter(Enter), .DataIn(DataIn), .Flags(Flags),
    .ToDisplay(ToDisplay), .CurrentState(CurrentState));
    
    // generate a clock signal that inverts its value every five time units
	always  #5 clk=~clk; // 100MHz
	
	//here we assign values to the inputs
	initial begin
	    clk = 0;
	    Enter = 0;
		resetN = 0;
        #10 resetN = 1;
        #10 DataIn = 16'hcade;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'hca5b;
        #10 resetN = 0; #200 resetN = 1; #200
        
        #10 DataIn = 16'hcade;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'hca5b;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0001;//resta
        #10 DataIn = 16'h0003;//and
        #10 Enter = 1; #500 Enter = 0;
        
        #100 Enter = 1; #500 Enter = 0; //Volver a estado Wait_OPA
        
        #10 DataIn = 16'heafb;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h1d4b;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0003;//and
        #10 DataIn = 16'h0002;//or
        #10 Enter = 1; #500 Enter = 0;
        
        #100 Enter = 1; #500 Enter = 0; //Volver a estado Wait_OPA
        
        #10 DataIn = 16'habcd;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'hfafb;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0002;//or
        #10 DataIn = 16'h0001;//resta
        #10 Enter = 1; #500 Enter = 0;
        
        #100 Enter = 1; #500 Enter = 0; //Volver a estado Wait_OPA
        
        #10 DataIn = 16'h415b;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h567f;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0002;//or
        #10 DataIn = 16'h0000;//suma
        #10 Enter = 1; #500 Enter = 0;
        
        #100 resetN = 0;
	end
endmodule

module actividad2();
    logic            clk, resetN;
    logic            Enter;
    logic    [15:0]  DataIn;
    logic            Undo;
    logic    [3:0]   Flags;
    logic    [15:0]  ToDisplay;
    logic    [1:0]   CurrentState;
    
    Act2_RPCalculator#(10) act2(.clk(clk), .resetN(resetN), .Enter(Enter), .Undo(Undo), .DataIn(DataIn),
     .Flags(Flags), .ToDisplay(ToDisplay), .CurrentState(CurrentState));
    
    // generate a clock signal that inverts its value every five time units
	always  #5 clk=~clk; // 100MHz
	
	//here we assign values to the inputs
	initial begin
	    clk = 0;
	    Undo = 0;
	    Enter = 0;
		resetN = 0;
        #10 resetN = 1;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'hcade;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'hca5b;
        #10 resetN = 0; #200 resetN = 1; #200
        
        #10 DataIn = 16'hcade;
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'hcaae;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'hca5b;
        #10 Enter = 1; #500 Enter = 0;
        
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'hca53;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0001;//resta
        #10 DataIn = 16'h0003;//and
        #10 Enter = 1; #500 Enter = 0;
        
        #100 Enter = 1; #500 Enter = 0; //Volver a estado Wait_OPA
        
        #10 DataIn = 16'heafb;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h1d4b;
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'h1c4b;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0003;//and
        #10 DataIn = 16'h0002;//or
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'h0002;//or
        #10 Enter = 1; #500 Enter = 0;
        
        #100 Enter = 1; #500 Enter = 0; //Volver a estado Wait_OPA
        
        #10 DataIn = 16'habcd;
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'haaaa;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'hfafb;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0002;//or
        #10 DataIn = 16'h0001;//resta
        #10 Enter = 1; #500 Enter = 0;
        
        #100 Enter = 1; #500 Enter = 0; //Volver a estado Wait_OPA
        
        #10 DataIn = 16'h415b;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h567f;
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'h5a7a;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0002;//or
        #10 DataIn = 16'h0000;//suma
        #10 Enter = 1; #500 Enter = 0;
        
        #100 resetN = 0;
	end
endmodule

module actividad3();
    logic            clk, resetN;
    logic            Enter;
    logic    [15:0]  DataIn;
    logic    [3:0]   Flags;
    logic    [1:0]   CurrentState;
    logic Undo, DisplayFormat;
    logic    [6:0]   Segments;
    logic    [4:0]   Anodes;
    
    Act3_RPCalculator#(10) act3(.clk(clk), .resetN(resetN), .Enter(Enter), .Undo(Undo), .DisplayFormat(DisplayFormat) , .DataIn(DataIn), .Flags(Flags),
    .CurrentState(CurrentState), .Segments(Segments), .Anodes(Anodes));
    
    // generate a clock signal that inverts its value every five time units
	always  #5 clk=~clk; // 100MHz
	
	//here we assign values to the inputs
	initial begin
	    clk = 0;
	    Undo = 0;
	    Enter = 0;
		resetN = 0;
		DisplayFormat = 0;
        #10 resetN = 1;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'hcade;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'hca5b;
        #10 resetN = 0; #200 resetN = 1; #200
        
        #10 DataIn = 16'hcade;
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'hcaae;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'hca5b;
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'hca53;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0001;//resta
        #10 DataIn = 16'h0003;//and
        #10 Enter = 1; #500 Enter = 0;
        
        #100 Enter = 1; #500 Enter = 0; //Volver a estado Wait_OPA
        
        DisplayFormat = 1;
        #10 DataIn = 16'heafb;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h1d4b;
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'h1c4b;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0003;//and
        #10 DataIn = 16'h0002;//or
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'h0002;//or
        #10 Enter = 1; #500 Enter = 0;
        
        #100 Enter = 1; #500 Enter = 0; //Volver a estado Wait_OPA
        
        DisplayFormat = 0;
        #10 DataIn = 16'habcd;
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        #10 DataIn = 16'haaaa;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'hfafb;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0002;//or
        #10 DataIn = 16'h0001;//resta
        #10 Enter = 1; #500 Enter = 0;
        DisplayFormat = 1;
        #100 Enter = 1; #500 Enter = 0; //Volver a estado Wait_OPA
        
        #10 DataIn = 16'h415b;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h567f;
        #10 Enter = 1; #500 Enter = 0;
        #10 Undo = 1; #500 Undo = 0;
        DisplayFormat = 0;
        #10 DataIn = 16'h5a7a;
        #10 Enter = 1; #500 Enter = 0;
        #10 DataIn = 16'h0002;//or
        #10 DataIn = 16'h0000;//suma
        #10 Enter = 1; #500 Enter = 0;
        
        #100 resetN = 0;
	end
endmodule