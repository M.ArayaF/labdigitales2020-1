`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2020 03:20:12
// Design Name: 
// Module Name: Bank_register
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//banco de registro con carga condicional
module Bank_register #(parameter C_WIDTH = 16)
(   input logic clk,
    input logic reset,
    input logic control,
    input logic [C_WIDTH-1:0] data_in,
    output logic [C_WIDTH-1:0]data_out);

    logic [C_WIDTH-1:0] data_aux;
    
    always_comb begin
        data_aux = 'b0;
        case(control)
            1'b1 : data_aux = data_in;
            1'b0 : data_aux = data_out;
        endcase  
    end
    
    always_ff@(posedge clk) begin
        if(reset) 
            data_out <= 'd0;
        else 
            data_out <= data_aux;
    end

endmodule
