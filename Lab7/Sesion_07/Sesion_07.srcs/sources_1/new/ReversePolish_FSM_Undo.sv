`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2020 04:23:06
// Design Name: 
// Module Name: ReversePolish_FSM_Undo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ReversePolish_FSM_Undo
(   input  logic       clock,
    input  logic       reset, TEnter, TUndo,
    output logic LoadOpA, LoadOpB, LoadOpcode, LoadFlag,
    output logic [1:0] CurrentState);

    enum logic [3:0] {Wait_OPA, Wait_OPB, Wait_OpCode, Show_result} state, next_state;
    
    always_comb begin
        next_state = state;
        LoadOpA = 1'b0;
        LoadOpB = 1'b0; 
        LoadOpcode = 1'b0; 
        LoadFlag = 1'b0;
        CurrentState = 2'b0;
        
        case (state)
            Wait_OPA: begin
                CurrentState = 2'b00;
                LoadOpA = 1'b1;
                if(TEnter == 1'b1) begin
                    next_state = Wait_OPB;
                end
            end
        
            Wait_OPB: begin
                CurrentState = 2'b01;
                LoadOpB = 1'b1;
                if(TEnter == 1'b1) begin
                    next_state = Wait_OpCode;
                end
                if(TUndo == 1'b1) begin
                    next_state = Wait_OPA;
                end
            end
        
            Wait_OpCode: begin
                CurrentState = 2'b10;
                LoadOpcode = 1'b1;
                if(TEnter == 1'b1) begin
                    next_state = Show_result;
                end
                if(TUndo == 1'b1) begin
                    next_state = Wait_OPB;
                end                
            end
        
            Show_result: begin
                CurrentState = 2'b11;
                LoadFlag = 1'b1;
                if(TEnter == 1'b1) begin
                    next_state = Wait_OPA;
                end
                if(TUndo == 1'b1) begin
                    next_state = Wait_OpCode;
                end                   
            end                
        endcase
    end	

    always_ff@(posedge clock)
        if(reset)
            state <= Wait_OPA;
        else
            state <= next_state;
    endmodule

