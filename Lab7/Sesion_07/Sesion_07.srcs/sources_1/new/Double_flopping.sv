`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2020 02:28:50
// Design Name: 
// Module Name: Double_flopping
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Double_flopping(
    input logic clk,
    input logic PB,
    output logic PB_sync
    );
    
// Double flopping stage for synchronizing asynchronous PB input signal
// PB_sync is the synchronized signal

    logic PB_sync_aux;
    always_ff @(posedge clk) begin
        PB_sync_aux <= PB;
        PB_sync     <= PB_sync_aux;
    end
endmodule
