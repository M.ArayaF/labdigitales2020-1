`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.07.2020 19:20:00
// Design Name: 
// Module Name: Act3_RPCalculator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Act3_RPCalculator
#(parameter N_debouncer = 10)
(   input logic             clk, resetN,
    input logic             Enter, Undo, DisplayFormat,
    input logic     [15:0]  DataIn,
    output logic    [3:0]   Flags,
    output logic    [1:0]   CurrentState,
    output logic    [6:0]   Segments,
    output logic    [4:0]   Anodes);
    
    logic push_enter, global_reset, push_undo;
    logic [15:0] bank_OpA, bank_OpB, result;
    logic [1:0] bank_OpCo;
    logic LoadOpA, LoadOpB, LoadOpcode, LoadFlag;
    logic [3:0]   Flags_aux;
    logic [15:0] ToDisplay;
    logic [31:0] out_DD, BCD_in;
    logic [7:0] original_anodes;
    
    //Df para el boton electromecanico resetN
    Double_flopping Reset_button (.clk(clk), .PB(resetN), .PB_sync(global_reset));    
    //Df + filtro anti rebote para el boton electromecanico Enter y Undo
    PB_Debouncer_FSM #(N_debouncer) Enter_button (.clk(clk), .rst(~global_reset), .PB(Enter), .PB_pressed_pulse(push_enter),
                                                  .PB_released_pulse(), .PB_pressed_status());
    PB_Debouncer_FSM #(N_debouncer) Undo_button (.clk(clk), .rst(~global_reset), .PB(Undo), .PB_pressed_pulse(push_undo),
                                                  .PB_released_pulse(), .PB_pressed_status());
    
    ReversePolish_FSM_Undo  Polish_u (.clock(clk), .reset(~global_reset), .TEnter(push_enter), .TUndo(push_undo) ,.LoadOpA(LoadOpA), 
                                      .LoadOpB(LoadOpB), .LoadOpcode(LoadOpcode), .LoadFlag(LoadFlag), .CurrentState(CurrentState));
                               
    Bank_register #(16) bank_A (.clk(clk), .reset(~global_reset), .control(LoadOpA), .data_in(DataIn), .data_out(bank_OpA));
    Bank_register #(16) bank_B (.clk(clk), .reset(~global_reset), .control(LoadOpB), .data_in(DataIn), .data_out(bank_OpB));
    Bank_register #(2) bank_Cnt (.clk(clk), .reset(~global_reset), .control(LoadOpcode), .data_in(DataIn[1:0]), .data_out(bank_OpCo));
        
    ALU_ref #(16) ALU (.A(bank_OpA), .B(bank_OpB), .OpCode(bank_OpCo), .Result(result), .Status(Flags_aux));
    
    Bank_register #(4) bank_flag (.clk(clk), .reset(~global_reset), .control(LoadFlag), .data_in(Flags_aux), .data_out(Flags));
    
    Display_selector D_selector (.ToDisplaySel(CurrentState), .data_in(DataIn), .Result(result), .ToDisplay(ToDisplay));
    double_dabble DD(.clk(clk),.trigger(1'd1),.in({16'd0,ToDisplay}),.bcd(out_DD), .idle());
    
    always_comb begin
        case(DisplayFormat)
            1'b0 : BCD_in = {16'd0,ToDisplay};
            1'b1 : BCD_in = out_DD;
        endcase
    end
    
    NumTo7Seg Displays(.clock(clk), .reset(~global_reset), .BCD_in(BCD_in), .segments(Segments), .anodos(original_anodes));
    
    assign Anodes = original_anodes[4:0];

endmodule
