`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2020 03:43:18
// Design Name: 
// Module Name: Display_selector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Display_selector
(   input logic [1:0] ToDisplaySel,
    input logic [15:0] data_in, Result,
    output logic [15:0] ToDisplay);
    
    always_comb begin
        case(ToDisplaySel)
            2'b00 : ToDisplay =  data_in;
            2'b01 : ToDisplay =  data_in; 
            2'b10 : ToDisplay =  {14'b0, data_in[1:0]};
            2'b11 : ToDisplay = Result;
        endcase  
    end
    
endmodule
