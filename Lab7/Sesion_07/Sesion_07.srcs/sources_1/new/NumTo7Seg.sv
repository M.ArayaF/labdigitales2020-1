`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.07.2020 13:17:50
// Design Name: 
// Module Name: NumTo7Seg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NumTo7Seg(
    input logic clock,
    input logic reset,
    input logic [31:0] BCD_in,
    output logic [6:0] segments, //CA,CB,CC,CD,CE,CF,CG,
    output logic [7:0] anodos //AN7, AN6, AN5, AN4, AN3, AN2, AN1, AN0
    );
    logic [2:0] contador_sync;
    logic [3:0] valor;
    
    //Contador que indicara cual sera el display prendido
    counter_Nbits #(.N(3)) contador_3bit (.clk(clock), .reset(reset), .count(contador_sync));
    //Demultiplexor que deteterminara que display se prendera o apagara
    Control_AN ctl_AN (.control_sync(contador_sync),.reset(reset) , .out(anodos));
    //Multiplexor que determinara cual es el valor que se debe mostrar en el display que este prendido
    Mux_display multiplexor(.control_sync(contador_sync), .valores(BCD_in), .out(valor));
    //Convesor de BCD a 7 segmentos en base al valor anterior
    BCD_to_7Seg conversor (.BCD_in(valor), .sevenSeg(segments));
endmodule

