`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.06.2020 14:32:26
// Design Name: 
// Module Name: Control_AN
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Control_AN(
    input logic [2:0] control_sync,
    input logic reset,
    output logic [7:0] out);
    
    always_comb begin
        if (reset) begin
            out = 8'b11111111;
        end
        else begin
            case(control_sync)
                // Los display se apagan con 1, y se prenden con 0
                3'b000 : out = 8'b11111110;
                3'b001 : out = 8'b11111101;
                3'b010 : out = 8'b11111011;
                3'b011 : out = 8'b11110111;
                3'b100 : out = 8'b11101111;
                3'b101 : out = 8'b11011111;
                3'b110 : out = 8'b10111111;
                3'b111 : out = 8'b01111111;
            endcase
        end
    end
endmodule