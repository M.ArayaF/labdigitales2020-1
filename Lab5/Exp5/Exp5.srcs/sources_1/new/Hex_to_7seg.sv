`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.06.2020 15:39:23
// Design Name: 
// Module Name: Hex_to_7seg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Hex_to_7seg(
    input logic clk,
    input logic reset,
    input logic [31:0] Result,
    output logic [6:0] Segments, //CA,CB,CC,CD,CE,CF,CG,
    output logic [7:0] Anodes //AN7, AN6, AN5, AN4, AN3, AN2, AN1, AN0
    );
    logic new_clock;
    logic [2:0] contador_sync;
    logic [3:0] valor;
    
    clock_divider #(.f_clk_out(480)) div_clock (.clk_in(clk), .reset(reset), .clk_out(new_clock));
    //Contador que indicara cual sera el display prendido
    counter_Nbits #(.N(3)) contador_3bit (.clk(new_clock), .reset(reset), .count(contador_sync));
    //Demultiplexor que deteterminara que display se prendera o apagara
    Control_AN ctl_AN (.control_sync(contador_sync),.reset(reset) , .out(Anodes));
    //Multiplexor que determinara cual es el valor que se debe mostrar en el display que este prendido
    Mux_display multiplexor(.control_sync(contador_sync), .valores(Result), .out(valor));
    //Convesor de BCD a 7 segmentos en base al valor anterior
    BCD_to_7Seg conversor (.BCD_in(valor), .sevenSeg(Segments));
endmodule
