`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.06.2020 16:00:38
// Design Name: 
// Module Name: FPGA_chip
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FPGA_chip #(parameter C_WIDTH = 8) (
    input logic [15:0] SW,
    input logic BTNC, BTNU, BTNL, BTNR,
    input logic CLK100MHZ,
    input logic CPU_RESETN,
    output logic CG, CF, CE, CD, CC, CB, CA,
    output logic [7:0] AN
    );
    logic [C_WIDTH-1:0] result;
    logic [3:0] status;
    logic [6:0] segments;
    logic [7:0] anodes;
    logic [1:0] OpCode;
    logic [C_WIDTH-1:0] A;
    logic [C_WIDTH-1:0] B;
    
    always_comb begin
    
    {B,A} = SW;
    
        case({BTNC, BTNU, BTNL, BTNR})
            4'b0001 : OpCode = 2'b00;
            4'b0010 : OpCode = 2'b01;
            4'b0100 : OpCode = 2'b10;
            4'b1000 : OpCode = 2'b11;
            default : OpCode = 2'b00;
        endcase
    end
    
    ALU_ref #(.C_WIDTH(C_WIDTH)) alu (.A(A),
 .B(B),
 .OpCode(OpCode),
 .Result(result),
 .Status(status));
    Hex_to_7seg hex (.clk(CLK100MHZ), .reset(CPU_RESETN), .Result(result), .Segments({CG, CF, CE, CD, CC, CB, CA}), .Anodes(AN));

endmodule
