`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.06.2020 15:25:20
// Design Name: 
// Module Name: ALU_register
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU_register( 
    input logic [15:0]data_in, 
    input logic load_A, 
    input logic load_B,
    input logic load_Op,
    input logic clk,
    input logic reset,
    input logic updateRes,
    output logic [6:0]segments,
    output logic [7:0]AN);
    
    logic [15:0]data_outA;
    logic [15:0]data_outB;
    logic [1:0]data_outOp;
    logic [15:0]data_outRes;
    logic [15:0]Result;
    logic [3:0]Status;
    
    Mux_Flip A(.data_in(data_in), .control(load_A), .reset(reset), .clk(clk), .data_out(data_outA));
    Mux_Flip B(.data_in(data_in), .control(load_B), .reset(reset), .clk(clk), .data_out(data_outB));
    Mux_Flip #(2)Op(.data_in(data_in[15:14]), .control(load_Op), .reset(reset), .clk(clk), .data_out(data_outOp));
    
    ALU_ref #(16) alu(.A(data_outA),
 .B(data_outB),
 .OpCode(data_outOp),
 .Result(Result),
 .Status(Status));
    
    Mux_Flip result(.data_in(Result), .control(updateRes), .clk(clk), .reset(reset), .data_out(data_outRes));
    
    Hex_to_7seg hex(.clk(clk), .reset(reset), .Result(data_outRes), .Segments(segments), .Anodes(AN));

endmodule

module Mux_Flip #(parameter C_WIDTH = 16)(
                input logic [C_WIDTH-1:0]data_in,
                input logic control,
                input logic reset,
                input logic clk,
                output logic [C_WIDTH-1:0]data_out);
    
    logic [C_WIDTH-1:0] data;
    
    always_comb begin
        data = '0;
        case(control)
            1'b1 : data = data_in;
            1'b0 : data = data_out;
        endcase  
    end
    
    always_ff@(posedge clk) begin
        if(reset) 
            data_out <= 8'd0;
        else 
            data_out <= data;
    end

endmodule
