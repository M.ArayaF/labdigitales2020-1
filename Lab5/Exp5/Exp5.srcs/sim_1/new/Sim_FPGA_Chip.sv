`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.06.2020 15:36:31
// Design Name: 
// Module Name: Sim_FPGA_Chip
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sim_FPGA_Chip();
    parameter C_WIDTH = 8;
    logic clk;
    logic reset;
    logic [C_WIDTH-1:0] A;
    logic [C_WIDTH-1:0] B;
    logic C, U, L, R;
    logic CG, CF, CE, CD, CC, CB, CA, AN;
    
    FPGA_chip #(.C_WIDTH(C_WIDTH)) chip(.SW('0),
    .BTNC(C), .BTNU(U), .BTNL(L), .BTNR(R),
    .CLK100MHZ(clk),
    .CPU_RESETN(reset),
    .CG(CG), .CF(CF), .CE(CE), .CD(CD), .CC(CC), .CB(CB), .CA(CA),
    .AN(AN));
    
    always #5 clk =~clk;
    
    initial begin
    clk = 1;
    reset = 1;
    A = 8'b1100_0000;
    B = 8'b1010_0000;
    {C, U, L, R} = 4'b0001;
    #10
    reset = 0;
    A = 8'b1110_0000;
    B = 8'b1100_0000;
    {C, U, L, R} = 4'b0001;
    #10 
    A = 8'b0101_0000;
    B = 8'b0110_0000;
    {C, U, L, R} = 4'b0001;
    #10 
    A = 8'b0000_0001;
    B = 8'b0000_0001;
    {C, U, L, R} = 4'b0010;
    #10 
    A = 8'b1110_1011;
    B = 8'b1101_0101;
    {C, U, L, R} = 4'b0010;
    #10 
    A = 8'b1100_1101;
    B = 8'b0111_0101;
    {C, U, L, R} = 4'b0010;         
    #10
    A = 8'b0111_0001;
    B = 8'b0110_1101;
    {C, U, L, R} = 4'b0100;
    #10
    A = 8'b1100_0101;
    B = 8'b0110_1101;
    {C, U, L, R} = 4'b0100;
    #10
    A = 8'b0001_0001;
    B = 8'b1110_1101;
    {C, U, L, R} = 4'b0100;
    #10
    A = 8'b0111_1101;
    B = 8'b1110_0101;
    {C, U, L, R} = 4'b0100;
    #10
    A = 8'b1111_0101;
    B = 8'b1100_1100;
    {C, U, L, R} = 4'b1000;
    #10
    A = 8'b1101_0101;
    B = 8'b1101_1100;
    {C, U, L, R} = 4'b1000;
    #10
    A = 8'b1111_0101;
    B = 8'b0101_1111;
    {C, U, L, R} = 4'b1000;
    #10
    A = 8'b0011_1101;
    B = 8'b1110_1100;
    {C, U, L, R} = 4'b1000;
    #10
    A = 8'b1111_0101;
    B = 8'b1110_1101;
    {C, U, L, R} = 4'b1000;
    #10
    A = 8'b1111_0101;
    B = 8'b0000_0000;
    {C, U, L, R} = 4'b1000;
    end
    
endmodule
