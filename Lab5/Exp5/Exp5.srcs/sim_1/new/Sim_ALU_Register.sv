`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.06.2020 16:38:10
// Design Name: 
// Module Name: Sim_ALU_Register
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sim_ALU_Register();
    logic [15:0]data_in;
    logic load_A;
    logic load_B;
    logic load_Op;
    logic clk = 0;
    logic reset;
    logic updateRes;
    logic [6:0]seg;
    logic [7:0]AN;
    
    ALU_register alu_reg(.data_in(data_in), .load_A(load_A), .load_B(load_B), .load_Op(load_Op), .clk(clk), .reset(reset), .updateRes(updateRes),
    .segments(seg), .AN(AN));
    
    always #5 clk = ~clk;
    
    initial begin
        reset = 1;
        #10
        reset = 0;
        #10
        updateRes = 0;
        data_in = '1;
        {load_A,load_B, load_Op} = '1;
        #10
        {load_A,load_B, load_Op} = '0;
        #10
        data_in = 16'hfafb;
        load_A = 1;
        #10
        {load_A, load_B} = 2'b01;
        #10
        data_in = 16'h0002;
        {load_B, load_Op} = 2'b01;
        #10
        load_Op = 0;
        updateRes = 1;
        #10
        data_in = 16'h0000;
    end

endmodule
