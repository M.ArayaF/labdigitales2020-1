`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.05.2020 16:21:35
// Design Name: 
// Module Name: Contador
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Contador #(parameter N=4)( 
    input   logic  clk, reset, enable, dec, load,
    input logic [N-1:0] load_value,
    output  logic   [N-1:0]   count = 0
    );
    logic [N-1:0] count_next = '0;
    
    always_comb
    begin
        if(enable == 1'b1)
        begin
            if(load == 1'b1)
            begin
                count_next = load_value;
            end
            
            else
            begin
                if(dec == 1'b1)
                begin
                    count_next = count - 1;
                end
                
                else
                begin
                    count_next = count + 1;
                end
            end
            
        end
        else
        begin
            count_next = count;
        end
    end
    
    always_ff @(posedge clk)
    begin
        if (reset == 1'b1)
            count <= {N{1'b0}}; //'0
        else
            count <= count_next;
    end
endmodule

