`timescale 1ns / 1ps// El tiempo se maneja en nanoSg, pero corre en picoSg #1 = 1000 ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.05.2020 23:11:13
// Design Name: 
// Module Name: display8_Seg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module display8_Seg(
    input logic CLK100MHZ,
    input logic [3:0] valor0,
    input logic [3:0] valor1,
    input logic [3:0] valor2,
    input logic [3:0] valor3,
    input logic [3:0] valor4,
    input logic [3:0] valor5,
    input logic [3:0] valor6,
    input logic [3:0] valor7,
    output logic CA,CB,CC,CD,CE,CF,CG,
    output logic [7:0] AN,
    output logic DP
    );
    
    logic clk_inter = 0;
    logic [2:0] cnt_sincro;
    logic [3:0] ordenador;
    assign DP = 1'b1;
    
    clk_Divider #(50) uut(.clk_in(CLK100MHZ), .clk_out(clk_inter)); // 1_000_000 Hz - 1MHz
    contador_4bits uut2(.clk(clk_inter), .out(cnt_sincro)); // 3bit
    Demux uut3(.control(cnt_sincro), .out(AN));
    
    Mux uut4(.control(cnt_sincro), .valor0(valor0), .valor1(valor1), .valor2(valor2), .valor3(valor3), .valor4(valor4), .valor5(valor5), .valor6(valor6), .valor7(valor7), .out(ordenador));
    BCD_to_Seven_Seg UTT(.BCD_in(ordenador), .sevenSeg({CG,CF,CE,CD,CC,CB,CA}));
endmodule
