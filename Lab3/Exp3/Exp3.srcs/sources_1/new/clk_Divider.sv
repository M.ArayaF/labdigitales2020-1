`timescale 1ns / 1ps 
// El tiempo se maneja en nanoSg, pero corre en picoSg #1 = 1000 ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.05.2020 23:07:44
// Design Name: 
// Module Name: clk_Divider
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clk_Divider#(parameter COUNTER_MAX = 100000)
                    (input logic clk_in,
                    output logic clk_out);
  
  localparam DELAY_WIDTH = $clog2(COUNTER_MAX); // The $clog2 function returns the ceiling(el n�mero entero mayor, m�s cercano) of the logarithm to the base 2.
  logic [DELAY_WIDTH-1:0] counter = 'd0;
  
  // Resetea el contador e invierte el valor del reloj de salida
  // cada vez que el contador llega a su valor maximo.
  
  always_ff @(posedge clk_in) begin
    if (counter == COUNTER_MAX -1) begin
     // Se resetea el contador y se invierte la salida 
        counter <= 'd0;
        clk_out <= ~clk_out;
        end 
   else begin
   // se incrementa el contador y se mantiene la salida con su valor anterior
        counter <= counter + 'd1;
        clk_out <= clk_out;
        end
   end
endmodule
