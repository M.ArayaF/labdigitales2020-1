`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.05.2020 15:27:36
// Design Name: 
// Module Name: Mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Mux(
    input logic [2:0] control,
    input logic [3:0] valor0,
    input logic [3:0] valor1,
    input logic [3:0] valor2,
    input logic [3:0] valor3,
    input logic [3:0] valor4,
    input logic [3:0] valor5,
    input logic [3:0] valor6,
    input logic [3:0] valor7,
    output logic [3:0] out
    );
    always_comb begin
        case(control)
            3'b000 : out = valor0;
            3'b001 : out = valor1;
            3'b010 : out = valor2;
            3'b011 : out = valor3;
            3'b100 : out = valor4;
            3'b101 : out = valor5;
            3'b110 : out = valor6;
            3'b111 : out = valor7;
        endcase
    end
endmodule

