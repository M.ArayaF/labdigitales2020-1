`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.05.2020 23:26:47
// Design Name: 
// Module Name: contador_4bits
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module contador_4bits(
    input logic clk,
    output logic [2:0] out = 2'd0
    );
    always_ff @(posedge clk) begin
        out = out + 1;
    end

endmodule
