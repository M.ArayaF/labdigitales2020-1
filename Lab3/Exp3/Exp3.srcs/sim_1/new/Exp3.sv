`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.05.2020 15:39:53
// Design Name: 
// Module Name: Exp3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Exp3(

    );
endmodule

module Display();
    logic CLK100MHZ = 0;
    logic [3:0] valor0 = 4'b0000;
    logic [3:0] valor1 = 4'b0001;
    logic [3:0] valor2 = 4'b0010;
    logic [3:0] valor3 = 4'b0011;
    logic [3:0] valor4 = 4'b0100;
    logic [3:0] valor5 = 4'b0101;
    logic [3:0] valor6 = 4'b0110;
    logic [3:0] valor7 = 4'b0111;
    logic CA,CB,CC,CD,CE,CF,CG;
    logic [7:0] AN;
    logic DP;
    
    display8_Seg D8_Seg(
    .CLK100MHZ(CLK100MHZ),
    .valor0(valor0),
    .valor1(valor1),
    .valor2(valor2),
    .valor3(valor3),
    .valor4(valor4),
    .valor5(valor5),
    .valor6(valor6),
    .valor7(valor7),
    .CA(CA),.CB(CB),.CC(CC),.CD(CD),.CE(CE),.CF(CF),.CG(CG),
    .AN(AN),
    .DP(DP)
    );
    always #5 CLK100MHZ =~CLK100MHZ;
    initial begin
    end
    
endmodule

module Mux_Sim();
    logic [2:0] control_f;
    logic val0,val1,val2,val3,val4,val5,val6,val7,outF;
    
    Mux Jiro(
        .control(control_f),
        .valor0(val0),
        .valor1(val1),
        .valor2(val2),
        .valor3(val3),
        .valor4(val4),
        .valor5(val5),
        .valor6(val6),
        .valor7(val7),
        .out(outF));
    
    initial begin
        control_f=3'b000;
        {val0,val1,val2,val3,val4,val5,val6,val7} = 8'b10101010;
        repeat(3'b111)
        begin
            #5
            control_f=control_f+3'b001;
        end
    end
endmodule

module contador_Sim #(parameter N=4);
    logic clk = 0, reset, enable, dec, load;
    logic [N-1:0] load_value = 4'b1111;
    logic [N-1:0] count;
    
    Contador Cake(.clk(clk),
                    .reset(reset),
                    .enable(enable),
                    .dec(dec),
                    .load(load),
                    .load_value(load_value),
                    .count(count));
                    
    always #5 clk = ~clk;
    initial begin
        count=3'b000;
        enable = 1'b1;
        {reset, dec, load} = 5'b000;
        #15
        dec = 1'b1;
        #5
        dec = 1'b0;
        #5
        reset = 1'b1;
        #5
        reset = 1'b0;
        #5
        load = 1'b1;
        #5
        load = 1'b0;
        
    end
                    
    
endmodule
