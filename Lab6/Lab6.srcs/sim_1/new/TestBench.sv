`timescale 1ns / 1ps

module semaforo_test();
  
    // We need to give values at the inputs, so we define them as registers  
	reg clock;
	reg reset;
	reg TA, TB;
	
	//The outputs are wires. We don't connect them to anything, but we need to 
	// declare them to visualize them in the output timing diagram
	wire [1:0] LA, LB;
	
	// an instance of the Device Under Test
	Semaforo_FSM DUT(
        .clock (clock),
        .reset (reset),
        .TA (TA),
        .TB (TB),
        .LA (LA),
        .LB (LB)
        );
            
	// generate a clock signal that inverts its value every five time units
	always  #25 clock=~clock;
	
	//here we assign values to the inputs
	initial begin
		clock = 1'b0;
		reset = 1'b0;
		TA = 1'b1;
		TB = 1'b0;
		#40 TB = 1'b1;
		#60 reset = 1'b1;
		#10 reset = 1'b0;
		
		
		#38 TA = 1'b0;
		    
		
		#206 TB = 1'b0;
		#302 TA = 1'b1;
		#40 TB = 1'b1;
	end

endmodule

module pos_edge_test();
	logic sig;         // Declare internal TB signal called sig to drive the sig pin of the design
	logic clk;         // Declare internal TB signal called clk to drive clock to the design
	logic out;
	logic rst;

	// Instantiate the design in TB and connect with signals in TB
	pos_edge_det ped0 (  .clk_100MHZ(clk),
    					 .reset(rst),
 			      		 .L(sig),
 			      		 .P(out));

	// Generate a clock of 100MHz
	always #5 clk = ~clk;

	// Drive stimulus to the design
	initial begin
		clk = 0;
		sig = 0;
		rst = 0;
		#15 sig = 1;
		#20 sig = 0;
		#15 sig = 1;
		#10 sig = 0;
		#20;
	end
endmodule