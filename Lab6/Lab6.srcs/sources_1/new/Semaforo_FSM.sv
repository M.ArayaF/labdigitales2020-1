`timescale 1ns / 1ps

module Semaforo_FSM #(parameter DELAY = 500_000_000)(
	input  logic       clock,
	input  logic       reset, TA, TB,
	output logic [2:0] LA, LB
    );
    
    
   (*fsm_encoding = "sequential"*) enum logic[3:0] {STATE_0, STATE_1, STATE_2, STATE_3} state, next_state;
    
    //output encoding
    localparam GREEN    = 3'b001;
    localparam YELLOW   = 3'b101;
    localparam RED      = 3'b100;
    localparam DELAY_WIDTH = $clog2(DELAY);
    
    logic [DELAY_WIDTH-1:0] delay_timer;
    
    always_ff @(posedge clock) begin
	   if (reset) delay_timer <= 0;
	   else if (state != next_state) delay_timer <= 0; //reset the timer when state changes
	   else delay_timer <= delay_timer + 1;
    end

    
    // one combinational block computes the next_state and outputs for the
    // current state
    always_comb begin
        //using default assignments here allows us to save space, helps on readability,
        //and reduces the changes of errors
        next_state = state;
    	LA = RED;
    	LB = RED;
    	
    	case (state)
    		STATE_0: begin
    			     LA = GREEN;
    			     if(TA == 1'b0) begin
    			       if(delay_timer >= DELAY-1)begin
    			 	       next_state = STATE_1; 
    			 	   end
    			 	   else begin
    			 	       next_state = state;
    			 	   end
    		         end         
    		end

            STATE_1: begin
                    LA = YELLOW;
                    if(delay_timer >= DELAY-1)begin
    			 	       next_state = STATE_2; 
    			 	   end
    			 	else begin
    			 	   next_state = state;
    			 	end
            end
            
            STATE_2: begin
                    LB = GREEN;
                    if(TB == 1'b0) begin
                       if(delay_timer >= DELAY-1)begin
    			 	       next_state = STATE_3; 
    			 	   end
    			 	   else begin
    			 	       next_state = state;
    			 	   end
                    end
            end

            STATE_3: begin
                    LB = YELLOW;
                if(TB == 1'b0) begin
                    if(delay_timer >= DELAY-1)begin
                         next_state = STATE_0; 
                    end
                    else begin
                         next_state = state;
                    end
                end
            end                
    	endcase
    end	

    //when clock ticks, update the state
    always_ff @(posedge clock or posedge reset)
    	if(reset)
    		state <= STATE_0;
    	else
    		state <= next_state;
endmodule
