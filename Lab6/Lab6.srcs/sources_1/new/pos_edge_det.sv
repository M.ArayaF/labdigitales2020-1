`timescale 1ns / 1ps

module pos_edge_det(
	input clk_100MHZ,
	input reset,
	input L, 
	output logic P
);
	enum logic [2:0]	{S0,
						 S1,
						 S2} state, next_state;

	always_ff@(posedge clk_100MHZ) begin 
		if(reset) begin 
			state <= S0;
		end
		else begin 
			state <= next_state;
		end
	end
	
	always_comb begin 
		next_state = S0;
		case(state) 
			S0 : if(L == 1)  begin 
					next_state = S1;
				 end
			S1 : if(L == 1) begin 
					next_state = S2;
				 end
			S2 : if(L == 1) begin 
					next_state = S2;
				 end
		endcase
	end
	
	always_comb begin 
		if(state == S1)
			P = 1'b1;
		else 
			P = 1'b0;
		
	end
	
endmodule 